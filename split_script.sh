#!/bin/bash

for file in mono/*.tif; do
    # Check if the file exists and is a regular file
    if [ -f "$file" ]; then
        # Extract the filename without the extension
        filename=$(basename -- "$file" .tif)
        
        # Run the convert command to extract the first page
        convert "$file"[0] -delete 1 "foo/${filename}.tif"
        
        # Optionally, you can remove the original file if you want
        # rm "$file"
    fi
done
