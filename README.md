# LifeAnalytics technical test


[![Developer](https://img.shields.io/badge/Developed_by-Nootaku_(MWattez)-informational?style=for-the-badge)](https://gitlab.com/Nootaku)<br/>
![Last_Update](https://img.shields.io/badge/Last_update-2024_May_13th-yellow?style=for-the-badge)

Create a **report** that recognizes _all cells_ in _all images_ of a sample data. ([link to the sample data](https://drive.google.com/drive/folders/11WboE7HZI5Q6ifT22VduaGZJI32A2D2k?usp=sharing))

The report should be created using [StarDist](https://github.com/stardist/stardist) - or any other similar library.

> Note:<br/>
> We will give you a reward if you submit an excellent report (original images and segmented images, their comparison data, and code for verification) within the deadline.

#### Author's Note

The Markdown in the Notebook does not display images on GitLab. However these are displayed properly within Jupyter Notebook or Jupyter Lab.